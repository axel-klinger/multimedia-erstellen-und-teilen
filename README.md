# Multimedia erstellen und teilen

Wie erstelle ich Bilder, Videos, interaktive Elemente etc für die Vermittlung von Wissen/Bildung

## Bilder

* Finden über Suchportale
* Zeichnen, Abfotografieren und Veröffentlichen
* Digital zeichnen und Veröffentlichen

## Videos

* Zeichnen und mit Handy filmen
* Screencasts
* Präsentationen erstellen und vertonen -> z.B. SlideWiki
* Explain Everything - oder andere Stift basierte Tools mit Aufzeichnung

## Interaktive Elemente

* Anleitungen von H5P etc
